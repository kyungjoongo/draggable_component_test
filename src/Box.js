import React from "react";

export default function Box(props) {

    return (
        <img alt={'box'} style={{
            position: 'absolute',
            left: props.posX,
            top: props.posY,
            width: 50,
            height: 50,
        }}
             id='drag1'
             draggable={true}
             src={'https://freeiconshop.com/wp-content/uploads/edd/box-outline-filled.png'}
        />
    )

}