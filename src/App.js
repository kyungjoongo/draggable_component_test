// @flow
import * as React from 'react';
import Draggable from "./Draggable";
import Box from "./Box";

type Props = {};
type State = {};


export default class App extends React.Component<Props, State> {

    render() {
        return (
            <Draggable color='#EBECF0'>
                {pos => (
                    <Box posX={pos.x} posY={pos.y}/>
                )}
            </Draggable>

        )
    }
};





