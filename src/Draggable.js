// @flow
import * as React from 'react';

type Props = {
    color: string,

};
type State = {
    posX: number,
    posY: number,

};


export default class Draggable extends React.Component<Props, State> {

    constructor() {
        super();
        this.state = {
            posX: 50,
            posY: 50,
        }
    }

    allowDrop(ev) {
        ev.preventDefault();
    }


    handleOnDrop(ev) {
        ev.preventDefault();
        let clientX = ev.clientX;
        let clientY = ev.clientY;
        this.setState({
            posX: clientX,
            posY: clientY,
        })

    }

    sendPosXY = value => {
        return value;
    }

    render() {
        return (
            <div id='grid'
                 style={{background: this.props.color, height: window.innerHeight, position: 'absolute', width: '100%'}}
                 onDrop={(ev) => {
                     this.handleOnDrop(ev)
                 }}
                 onDragOver={(ev) => {
                     this.allowDrop(ev)
                 }}
            >
                {this.props.children(this.sendPosXY({x: this.state.posX, y: this.state.posY}))}
            </div>


        )
    }

};



